let audioContext;
let oscillator;
let audioInterval;

window.addEventListener("load", showMetronome);
document.getElementById("time-signature-button-group").addEventListener("click", setActiveTimeSignature);

function metronomeController() {
  const playPauseButton = document.getElementById("play-button");
  let metronomeEnabled = playPauseButton.dataset.metronomeEnabled;
  if (metronomeEnabled === "false") {
    playPauseButton.setAttribute("data-metronome-enabled", "true");
    playSounds();
  } else {
    stopSounds();
    playPauseButton.setAttribute("data-metronome-enabled", "false");
  }

  changePlayPauseButtonIcon(metronomeEnabled);
}

function playSounds() {
  let bpm = $("#slider").roundSlider("getValue");
  const activeTimeSignature = document.querySelector("[data-active]").innerText;
  const beatsPerBar = activeTimeSignature[0];
  const referenceNote = activeTimeSignature[2];
  let beatCount = 0;
  let interval = 60 * 1000 / bpm; // interval in ms between beeps
  if (referenceNote === "8") interval /= 2;

  audioContext = new AudioContext();
  audioInterval = window.setInterval(() => {
    beatCount++;
    let frequency = 440;
    if (beatCount === 1 || (beatCount - 1) % beatsPerBar === 0) frequency = 880;
    oscillator = new OscillatorNode(audioContext, {type: "sine", frequency: frequency});
    oscillator.connect(audioContext.destination);
    oscillator.start();
    oscillator.stop(audioContext.currentTime + 0.05);
    oscillator = null;
  }, interval);
}

function stopSounds() {
  let metronomeEnabled = document.getElementById("play-button").dataset.metronomeEnabled;
  if (metronomeEnabled === "true") {
    window.clearInterval(audioInterval);
  }
}

function setActiveTimeSignature(event) {
  if (event.target.className === "time-signature-button") {
    let timeSignatureButtons = document.querySelectorAll(".time-signature-button");
    timeSignatureButtons.forEach(button => {
      if (button.hasAttribute("data-active")) button.removeAttribute("data-active");
    })

    event.target.dataset.active = "true";
    let metronomeEnabled = document.getElementById("play-button").dataset.metronomeEnabled;
    if (metronomeEnabled === "true") {
      stopSounds();
      playSounds();
    }
  }
}

function changePlayPauseButtonIcon(metronomeEnabled) {
  const playPauseButtonIcon = document.getElementById("play-button-icon");
  metronomeEnabled === "false"
    ? playPauseButtonIcon.innerText = "pause"
    : playPauseButtonIcon.innerText = "play_arrow";
}

function showMetronome() {
  document.fonts.ready.then(function() { // bug fix, font loading late
    let loadScreen = document.getElementById("load-screen");
    loadScreen.className = "fade";
    window.setTimeout(() => loadScreen.className = "hide", 500);
  })
}

// RoundSlider setup and config
$("#slider").roundSlider({
  sliderType: "min-range",
  svgMode: true,
  radius: 130,
  value: 120,
  max: 250,
  min: 20,
  startAngle: 90,
  animation: false,
  editableTooltip: false,
  keyboardAction: false,
  width: 5,
  borderWidth: 1,
  handleSize: "+11",
  tooltipFormat: function(input) {
    const bpmDiv = document.createElement("div");
    bpmDiv.id = "bpm";
    bpmDiv.textContent = `${input.value}`

    const bpmLabelDiv = document.createElement("div");
    bpmLabelDiv.id = "bpm-label";
    bpmLabelDiv.textContent = "BPM"

    bpmDiv.append(bpmLabelDiv);
    return bpmDiv;
  },
  pathColor: "232323",
  rangeColor: "177DD6",
  borderColor: "232323",
});

$("#slider").roundSlider({ beforeValueChange: "stopSounds" });
$("#slider").roundSlider({ change: "adjustBPM" });

function adjustBPM() {
  let metronomeEnabled = document.getElementById("play-button").dataset.metronomeEnabled;
  if (metronomeEnabled === "true") playSounds();
}
