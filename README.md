# README #

This digital metronome makes use of the Web Audio API. A different tone will occur on the downbeat of each measure, which is set by using the time signature.

![picture](/metronome-screenshot.png)

Copyright © 2020. All rights reserved.
